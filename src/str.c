#include "chnlib/str.h"

#include <chnlib/logger.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>


String str_init(const usize len) {
    char* ptr = malloc(sizeof(char) * len + 1);
    ptr[len] = '\0';

    return (String){ ptr, len };
}

void str_deinit(String str) {
    free(str.ptr);

    str.len = 0;
    str.ptr = null;
}

String str_slice(char* ptr, const usize len) {
    return (String){ ptr, len };
}

String str_copy(String str) {
    char* copy = malloc(sizeof(char) * str.len + 1);
    strncpy(copy, str.ptr, str.len);
    copy[str.len] = '\0';

    return (String){ copy, str.len };
}

__attribute__((format(printf, 1, 2)))
String str_from_format(const char *format, ...) {
    char buffer[1024];
    va_list args;
    va_start(args, format);

    int buffer_len = vsnprintf(buffer, 1024, format, args);

    va_end(args);

    if (buffer_len < 0) { chn_error("vsnprintf failed"); }

    char *ptr = malloc(sizeof(char) * buffer_len + 1);
    strncpy(ptr, buffer, buffer_len);
    ptr[buffer_len] = '\0';

    return (String){ ptr, buffer_len };
}

void str_append(String* str, String to_append) {
    usize new_len = str->len + to_append.len;
    str->ptr = realloc(str->ptr, new_len + 1);
    strcpy(str->ptr + str->len, to_append.ptr);
    str->len = new_len;
}

bool str_eq(const String a, const String b) {
    if (a.len != b.len) { return false; }

    for (usize i = 0; i < a.len; i += 1) {
        if (a.ptr[i] != b.ptr[i]) { return false; }
    }

    return true;
}
