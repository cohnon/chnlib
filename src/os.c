#include "chnlib/os.h"

#include <chnlib/logger.h>
#include <stdio.h>

void os_path_join(String *base, String add) {
    bool has_slash_suffix = base->ptr[base->len] == '/';

    usize new_len = base->len + add.len + (has_slash_suffix ? 0 : 1);
    String new = str_init(new_len);
    for (usize i = 0; i < base->len; i += 1) {
        new.ptr[i] = base->ptr[i];
    }

    if (base->len > 0 and not has_slash_suffix) {
        new.ptr[base->len] = '/';
    }

    usize add_index = has_slash_suffix ? base->len : base->len + 1;
    for (usize i = add_index; i < new_len; i += 1) {
        new.ptr[i] = add.ptr[i - add_index];
    }

    str_deinit(*base);

    *base = new;
}

String os_path_dir(String path) {
    usize new_len = path.len;
    for (usize i = path.len - 1; i --> 0;) {
        if (path.ptr[i] == '/') {
            new_len = i;
            break;
        }
    }

    return (String){ path.ptr, new_len };
}

bool os_read_file(String *out, String path) {
    FILE *file = fopen(path.ptr, "rb");
    if (file == null) {
        return false;
    }

    if (fseek(file, 0, SEEK_END) != 0) {
        return false;
    }

    isize file_len = ftell(file);
    if (file_len == -1) {
        return false;
    }

    char *ptr = malloc(sizeof(char)  *file_len + 1);
    if (ptr == null) {
        return false;
    }

    if (fseek(file, 0, SEEK_SET) != 0) {
        free(ptr);
        return false;
    }

    usize file_read = fread(ptr, sizeof(char), file_len, file);
    if (file_read != (usize)file_len) {
        free(ptr);
        return false;
    }

    fclose(file);

    *out = str_slice(ptr, file_len);

    return true;
}

bool os_create_file(String path, String content) {
    FILE *file = fopen(path.ptr, "wb");
    if (file == null) {
        return false;
    }

    fwrite(content.ptr, content.len, 1, file);

    fclose(file);

    return true;
}

bool os_path_exists(String path) {
    FILE *file = fopen(path.ptr, "r");
    if (file == null) { return false; }

    fclose(file);

    return true;
}
