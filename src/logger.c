#include "chnlib/logger.h"

#include <stdio.h>
#include <stdarg.h>

typedef struct {
    ChnLogLevel log_level;
} ChnLogger;

static ChnLogger logger;
static char* LOG_LEVEL_DISPLAY[] = {  "DEBUG", "INFO", "WARN", "ERROR" };
static char* LOG_LEVEL_COLOR[] =   {   GREEN,   BLUE,  YELLOW,  RED    };

void chn_log_level(ChnLogLevel log_level) {
    logger.log_level = log_level;
}

void internal__chn_log(
    const ChnLogLevel level,
    const usize line_number,
    const char* const file_name,
    const char* const format,
    ...
) {
    if (level < logger.log_level) { return; }

    // print header
    fprintf(
        stderr,
        "%s %s " RESET WHITE " %s:%zu " RESET " ",
        LOG_LEVEL_COLOR[level],
        LOG_LEVEL_DISPLAY[level],
        file_name,
        line_number
    );

    // print formatted string
    va_list args;
    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);

    fprintf(stderr, "\n");
}
