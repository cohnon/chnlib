#ifndef CHN_MAYBE_H
#define CHN_MAYBE_H

#include "chntype.h"

#define try(maybe_expr) __extension__           \
({                                              \
    __typeof__(maybe_expr) maybe = maybe_expr;  \
    if (maybe == 0) { return 0; }               \
    maybe;                                      \
})

#define unwrap(maybe) (*maybe)

#endif
