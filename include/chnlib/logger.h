#ifndef CHN_LOGGER_H
#define CHN_LOGGER_H

#include "chntype.h"

#define RESET   "\033[0m"
#define RED     "\033[41;1;97m"
#define GREEN   "\033[42;1;97m"
#define YELLOW  "\033[43;1;30m"
#define BLUE    "\033[44;1;97m"
#define WHITE   "\033[107;30m"

typedef enum {
    Chn_LogLevel_Debug,
    Chn_LogLevel_Info,
    Chn_LogLevel_Warning,
    Chn_LogLevel_Error,
} ChnLogLevel;

void chn_log_level(ChnLogLevel log_level);

__attribute__((format(printf, 4, 5)))
void internal__chn_log(
    const ChnLogLevel level,
    const usize line_number,
    const char* const file_name,
    const char* const format,
    ...
);

#define chn_debug(...) internal__chn_log(Chn_LogLevel_Debug, __LINE__, __FILE__, __VA_ARGS__)
#define chn_info(...) internal__chn_log(Chn_LogLevel_Info, __LINE__, __FILE__, __VA_ARGS__)
#define chn_warn(...) internal__chn_log(Chn_LogLevel_Warning, __LINE__, __FILE__, __VA_ARGS__)
#define chn_error(...) internal__chn_log(Chn_LogLevel_Error, __LINE__, __FILE__, __VA_ARGS__); exit(1);

#endif
