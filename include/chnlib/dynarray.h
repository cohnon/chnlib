#ifndef CHN_DYNARRAY_H
#define CHN_DYNARRAY_H

#include "chntype.h"

#define DynArray(T) T*

void* dynarray_init(void);
void* internal__dynarray_init_with(usize size, usize len);
void dynarray_deinit(void* array);

#define dynarray_init_with(T, len) internal__dynarray_init_with(sizeof(__typeof__(T)), len);

void  internal__dynarray_push(void** const array, const usize size, const void* const value);
#define dynarray_push(array, value) do { \
    __typeof__(array) tmp = value;       \
    internal__dynarray_push(             \
        (void** const)&array,            \
        sizeof(__typeof__(*array)),                  \
        tmp                              \
    );                                   \
} while(0)

#define dynarray_push_ptr(array, val) dynarray_push(array, &(__typeof__(*array)){&val})

void* internal__dynarray_add(void** const array, const usize size);
#define dynarray_add(array) (__typeof__(array))internal__dynarray_add((void** const)&array, sizeof(__typeof__(*array)))
void dynarray_pop(void* const array);

#define dynarray_last(array) array[dynarray_len(array) - 1]
usize dynarray_len(const void* const array);

#define dynarray_foreach(array, i) for (usize i = 0; i < dynarray_len(array); i += 1)

#endif
