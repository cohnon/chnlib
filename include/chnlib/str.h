#ifndef CHN_STRING_H
#define CHN_STRING_H

#include "chntype.h"

typedef struct {
    char* ptr;
    usize len;
} String;

String str_init(const usize len);
void str_deinit(String str);
String str_slice(char* ptr, const usize len);
String str_copy(String str);

// macro for comptime sizeof
#define str_from_lit(literal) (String){ literal, sizeof(literal) - 1 }
#define str_from_cstr(cstr) (String){ cstr, strlen(cstr) }

__attribute__((format(printf, 1, 2)))
String str_from_format(const char *format, ...);

void str_append(String* str, String to_append);
bool str_eq(const String a, const String b);

// to be used in formatted string. e.g. printf(".*s\n", str_format(foo));
#define str_format(str) (int)(str).len, (str).ptr

#endif
