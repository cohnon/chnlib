#ifndef CHN_OS_H
#define CHN_OS_H

#include "str.h"
#include "maybe.h"

void os_path_join(String *base, String add);
String os_path_dir(String path);

bool os_read_file(String *out, String path);
bool os_create_file(String path, String content);
bool os_path_exists(String path);

#endif
