HFILES = $(wildcard include/chnlib/*.h include/chnlib/*/*.h)

CFILES = $(wildcard src/*.c src/*/*.c)
OFILES = $(patsubst src/%.c, build/%.o, $(CFILES))

TESTFILES = $(wildcard test/*.test.c)
TESTEXES = $(patsubst test/%.c, build/%, $(TESTFILES))


CC = gcc
AR = ar
FLAGS = -std=c11 -O3 -Wall -Wextra -pedantic
INCLUDES = -Iinclude

ifeq ($(PREFIX),)
	PREFIX := /usr/local
endif


# =============
# build targets
# =============
default: build/libchn.a

# testing
test: $(TESTEXES)

build/%.test: $(CFILES) test/%.test.c
	mkdir -p $(@D)
	$(CC) $(FLAGS) -Iinclude $^ -o $@


# building
install: build/libchn.a
	mkdir -p $(DESTDIR)$(PREFIX)/lib/
	cp $< $(DESTDIR)$(PREFIX)/lib/
	mkdir -p $(DESTDIR)$(PREFIX)/include/
	cp -r include/chnlib $(DESTDIR)$(PREFIX)/include/

uninstall:
	rm $(DESTDIR)$(PREFIX)/lib/libchn.a
	rm -r $(DESTDIR)$(PREFIX)/include/chnlib

build/libchn.a: $(OFILES)
	$(AR) -rc $@ $^

build/%.o: src/%.c
	mkdir -p $(@D)
	gcc -c $(FLAGS) $(INCLUDES) $< -o $@

clean:
	rm -r build
